<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GviajeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'user_id' => $row->user_id,
                'order_id' => $row->order_id,
                'descripcion' => $row->descripcion,
                'tipo' => $row->tipo,
                'monto' => $row->monto,  
                'filename' => $row->filename,    
                'galones' => $row->galones,    
                'precio' => $row->precio,               
                'documents_id' => $row->documents_id,
                'fecha' => $row->created_at->format('Y-m-d H:i:s'),
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
